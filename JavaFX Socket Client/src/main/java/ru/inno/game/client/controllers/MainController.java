package ru.inno.game.client.controllers;

import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import ru.inno.game.client.sockets.SocketClient;
import ru.inno.game.client.utils.GameUtil;
import java.net.URL;
import java.util.ResourceBundle;

// контроллер осуществляет взаимодействие со сценой
public class MainController implements Initializable {

    private GameUtil gameUtil;

    @FXML
    private Button hiButton;

    @FXML
    private Label messageLabel;

    @FXML
    private TextField nameTextField;

    @FXML
    private Pane player;

    @FXML
    private Pane enemy;

    @FXML
    private Label hpPlayer;

    @FXML
    private Label hpEnemy;

    @FXML
    private AnchorPane pane;

    @FXML
    private Pane gameOver;

    private SocketClient client;

    //Timer на выстрел.
    private long lastShotTime=0;


    private final EventHandler<KeyEvent> playerControlEventHandler = event -> {
        if (event.getCode() == KeyCode.RIGHT) {
            if (player.getLayoutX() < 490) {
                gameUtil.goRight(player);
                client.sendMessage("right");
            }
        } else if (event.getCode() == KeyCode.LEFT) {
            if (player.getLayoutX() > 25) {
                gameUtil.goLeft(player);
                client.sendMessage("left");
            }
        } else if (event.getCode() == KeyCode.SPACE && (System.currentTimeMillis() - lastShotTime >= 2000)) {
            gameUtil.createBullet(player, enemy, false);
            client.sendMessage("shot");
            lastShotTime=System.currentTimeMillis();
        }
    };

    public EventHandler<KeyEvent> getPlayerControlEventHandler() {
        return playerControlEventHandler;
    }

    // инициализирует контроллер
    public void initialize(URL location, ResourceBundle resources) {
        // что должно происходить, когда мы нажимаем на кнопку?
        hiButton.setOnAction(event -> {
            // получаем текст, который ввел пользователь
            String name = nameTextField.getText();
            // формируем текст сообщения
            String message = "Привет, " + name + "!";
            // кладем текст в label
            messageLabel.setText(message);
            // сместить фокус с поля для ввода на всю сцену
            hiButton.getScene().getRoot().requestFocus();
            this.client = new SocketClient(this, "localhost", 9999);
            this.gameUtil = new GameUtil(this);
            this.client.setGameUtil(gameUtil);
            this.gameUtil.setSocketClient(this.client);
            this.client.sendMessage("nickname " + name);
            this.client.start();
        });

    }

    public Pane getGameOver() {
        return gameOver;
    }

    public Label getHpPlayer() {
        return hpPlayer;
    }

    public Label getHpEnemy() {
        return hpEnemy;
    }

    public AnchorPane getPane() {
        return pane;
    }

    public Pane getPlayer() {
        return player;
    }

    public Pane getEnemy() {
        return enemy;
    }


}
