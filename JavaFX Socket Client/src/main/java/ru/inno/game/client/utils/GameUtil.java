package ru.inno.game.client.utils;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.util.Duration;
import ru.inno.game.client.controllers.MainController;
import ru.inno.game.client.sockets.SocketClient;

public class GameUtil {

    private final static int DAMAGE = 5;
    private final static int PLAYER_STEP = 5;

    private final AnchorPane pane;
    private final Label hpPlayer;
    private final Label hpEnemy;
    private SocketClient socketClient;
    private final Pane gameOver;

    public GameUtil(MainController controller) {
        this.pane = controller.getPane();
        this.hpPlayer = controller.getHpPlayer();
        this.hpEnemy = controller.getHpEnemy();
        this.gameOver = controller.getGameOver();
    }

    public void setSocketClient(SocketClient socketClient) {
        this.socketClient = socketClient;
    }

    // shooterIsEnemy - true - враг стреляет в нас
    public void createBullet(Pane shooter, Pane target, boolean shooterIsEnemy) {
        Image image = new Image("/bullet.png");
        ImageView bullet = new ImageView(image);
        bullet.setLayoutX(shooter.getLayoutX()+15);
        if (shooterIsEnemy) {
            bullet.setLayoutY(shooter.getLayoutY() + 37);
        } else {
            bullet.setLayoutY(shooter.getLayoutY() - 37);
        }
        pane.getChildren().add(bullet);

        int value;
        Label hpLabel;

        if (shooterIsEnemy) {
            hpLabel = hpPlayer;
            value = 1;
            bullet.setRotate(180);
        } else {
            hpLabel = hpEnemy;
            value = -1;
        }

        Timeline timeline = new Timeline(new KeyFrame(Duration.seconds(0.005), animation -> {
            bullet.setLayoutY(bullet.getLayoutY() + value);
            if (bullet.getLayoutY() < 0 && bullet.getLayoutY() > 450) {
                pane.getChildren().remove(bullet);
            }
            if (bullet.isVisible() && isIntersects(bullet, target)) {
                bullet.setVisible(false);
                pane.getChildren().remove(bullet);
                createDamage(hpLabel);

                if (!shooterIsEnemy) {
                    socketClient.sendMessage("damage");
                }
            }

            if (Integer.parseInt(getHpEnemy().getText()) <= 0) {
                socketClient.sendMessage("STOP");
            }
        }));

        timeline.setCycleCount(470);
        timeline.play();
    }

    private boolean isIntersects(ImageView bullet, Pane target) {
        return bullet.getBoundsInParent().intersects(target.getBoundsInParent());
    }

    private void createDamage(Label hpLabel) {
        int hpPerson = Integer.parseInt(hpLabel.getText());
        if (hpPerson > 0) {
            hpLabel.setText(String.valueOf(hpPerson - DAMAGE));
        }
    }

    public void createGameOver() {
        gameOver.setVisible(true);
        Button button = new Button("Выход");
        button.resize(250, 150);
        button.setLayoutX(324);
        button.setLayoutY(350);
        pane.getChildren().add(button);
        button.setOnAction(event -> System.exit(0));

    }

    public void goLeft(Pane person) {
        person.setLayoutX(person.getLayoutX() - PLAYER_STEP);
    }

    public void goRight(Pane person) {
        person.setLayoutX((person.getLayoutX() + PLAYER_STEP));
    }

    public Label getHpEnemy() {
        return hpEnemy;
    }

}
