package ru.inno.game.server.repositories;

import ru.inno.game.server.models.Game;

public interface GamesRepository {
    void save(Game game);

    Game getById(Long gameId);

    void update(Game game);
}
