package ru.inno.game.server.repositories;

import ru.inno.game.server.models.Shot;

public interface ShotsRepository {
    void save(Shot shot);
}
