package ru.inno.game.server.repositories;

import ru.inno.game.server.models.Player;

import javax.sql.DataSource;
import java.sql.*;
import java.util.Optional;

public class PlayersRepositoryJdbcImpl implements PlayersRepository {

    //language=SQL
    private static final String SQL_FIND_BY_NICKNAME = "select id, ip, name, points, max_wins_count,max_loses_count from player where name = ?";

    //language=SQL
    private static final String SQL_INSERT = "insert into player(name, ip) values (?, ?)";

    //language=SQL
    private static final String SQL_UPDATE = "update player set ip=?,points=?,max_loses_count=?,max_wins_count=? where id=?";

    //language=SQL
    private static final String SQL_GET_BY_ID = "select id,ip,name,points,max_loses_count,max_wins_count from player where id=?";

    private final DataSource dataSource;

    public PlayersRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public Optional<Player> findByNickname(String nickname) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_FIND_BY_NICKNAME)) {
            statement.setString(1, nickname);
            return getOptionalPlayer(statement);
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void save(Player player) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, player.getNickname());
            statement.setString(2, player.getIp());

            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("Can't insert player");
            }

            ResultSet generatedKeys = statement.getGeneratedKeys();

            if (generatedKeys.next()) {
                player.setId(generatedKeys.getLong("id"));
            } else {
                throw new SQLException("Can't obtain id");
            }

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }


    //Полностью сам
    @Override
    public void update(Player player) {
        System.out.println(player);
        try (Connection connection = dataSource.getConnection(); PreparedStatement statement = connection.prepareStatement(SQL_UPDATE)) {
            statement.setString(1, player.getIp());
            statement.setInt(2, player.getPoints());
            statement.setInt(3, player.getLosesCount());
            statement.setInt(4, player.getWinsCount());
            statement.setLong(5, player.getId());
            if (statement.executeUpdate() == 0) {
                throw new IllegalStateException("Update failed");
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    //сам
    @Override
    public Optional<Player> getById(Long playerId) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_GET_BY_ID)) {
            statement.setLong(1, playerId);
            return getOptionalPlayer(statement);
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    //сам
    //пробросил исключение потому что метод, который вызывает эту функцию уже имеет try/catch
    private Optional<Player> getOptionalPlayer(PreparedStatement statement) throws SQLException {
        try (ResultSet result = statement.executeQuery()) {
            if (result.next()) {
                return Optional.of(Player.builder()
                        .id(result.getLong("id"))
                        .nickname(result.getString("name"))
                        .ip(result.getString("ip"))
                        .points(result.getInt("points"))
                        .losesCount(result.getInt("max_loses_count"))
                        .winsCount(result.getInt("max_wins_count"))
                        .build());
            } else {
                return Optional.empty();
            }
        }
    }

}
