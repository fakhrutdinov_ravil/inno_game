package ru.inno.game.server.services;

import ru.inno.game.server.models.Game;
import ru.inno.game.server.models.Player;
import ru.inno.game.server.models.Shot;
import ru.inno.game.server.repositories.GamesRepository;
import ru.inno.game.server.repositories.PlayersRepository;
import ru.inno.game.server.repositories.ShotsRepository;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Optional;

public class GameServiceImpl implements GameService {

    private final PlayersRepository playersRepository;

    private final GamesRepository gamesRepository;

    private final ShotsRepository shotsRepository;

    public GameServiceImpl(PlayersRepository playersRepository, GamesRepository gamesRepository, ShotsRepository shotsRepository) {
        this.playersRepository = playersRepository;
        this.gamesRepository = gamesRepository;
        this.shotsRepository = shotsRepository;
    }

    @Override
    public Long startGame(String firstIp, String secondIp, String firstPlayerNickname, String secondPlayerNickname) {
        // получили первого игрока
        Player first = checkIxExists(firstIp, firstPlayerNickname);
        // получили второго игрока
        Player second = checkIxExists(secondIp, secondPlayerNickname);
        // создаем игру
        Game game = Game.builder()
                .playerFirst(first)
                .playerSecond(second)
                .secondsGameTimeAmount(0L)
                .playerFirstShotsCount(0)
                .playerSecondShotsCount(0)
                .dateTime(LocalDateTime.now())
                .build();
        gamesRepository.save(game);
        return game.getId();
    }


    //Реализовал сам
    @Override
    public void shot(Long gameId, String shooterNickname, String targetNickname) {
        // получаем информацию об игроках
        Optional<Player> shooterFromRepository = playersRepository.findByNickname(shooterNickname);
        Player shooter;
        if (shooterFromRepository.isPresent()) {
            shooter = shooterFromRepository.get();
        } else {
            throw new IllegalStateException("Не удалось получить shooter из БД");
        }
        Optional<Player> targetFromRepository = playersRepository.findByNickname(targetNickname);
        Player target;
        if (targetFromRepository.isPresent()) {
            target = targetFromRepository.get();
        } else {
            throw new IllegalStateException("Не удалось получить target из БД");
        }

        // находим игру
        Game game = gamesRepository.getById(gameId);
        // создаем выстрел
        Shot shot = Shot.builder()
                .shooter(shooter)
                .target(target)
                .dateTime(LocalDateTime.now())
                .game(game)
                .build();
        // увеличили очки у того, кто стрелял
        shooter.setPoints(shooter.getPoints() + 1);
        // если стрелял первый игрок
        if (game.getPlayerFirst().getId().equals(shooter.getId())) {
            // увеличиваем количество попаданий в этой игре
            game.setPlayerFirstShotsCount(game.getPlayerFirstShotsCount() + 1);
        }

        // если стрелял второй игрок
        if (game.getPlayerSecond().getId().equals(shooter.getId())) {
            game.setPlayerSecondShotsCount(game.getPlayerSecondShotsCount() + 1);
        }

        // обновляем данные по стреляющему
        playersRepository.update(shooter);
        // обновляем данные по игре
        gamesRepository.update(game);
        // сохраняем выстрел
        shotsRepository.save(shot);
    }

    @Override
    public void stopGame(Long gameId, String nicknameOfWinner) {
        Player firstPlayer;
        Player secondPlayer;

        Game game = gamesRepository.getById(gameId);

        Duration duration = Duration.between(game.getDateTime().toLocalTime(), LocalTime.now());
        game.setSecondsGameTimeAmount(duration.getSeconds());

        //Обновили текущую игру
        gamesRepository.update(game);

        //Получили id игроков, которые играли.
        Long firstPlayerId = game.getPlayerFirst().getId();
        Long secondPlayerId = game.getPlayerSecond().getId();

        //Получили игроков из репозитория по ID
        Optional<Player> firstOptionalPlayer = playersRepository.getById(firstPlayerId);
        if (firstOptionalPlayer.isPresent()) {
            firstPlayer = firstOptionalPlayer.get();
        } else {
            throw new IllegalStateException("Первого игрока нет в репозитории");
        }
        Optional<Player> secondOptionalPlayer = playersRepository.getById(secondPlayerId);
        if (secondOptionalPlayer.isPresent()) {
            secondPlayer = secondOptionalPlayer.get();
        } else {
            throw new IllegalStateException("Второго игрока нет в репозитории");
        }

        //Получили игрока победителя
        Optional<Player> winnerPlayer = playersRepository.findByNickname(nicknameOfWinner);
        Long winnerId=null;
        if (winnerPlayer.isPresent()) {
            winnerId = winnerPlayer.get().getId();
        }
        //Сравниваем ID первого игрока с ID игроком победителем.
        //Если ID совпали, то увеличиваем кол-во побед у первого игрока на (предыдущее значение +1)
        //А у второго игрока увеличиваем кол-во поражений.
        //Иначе если ID второго игрока совпал, то соответственно у второго игрока увеличиваем победы
        //А у первого кол-во поражений.
        if (firstPlayerId.equals(winnerId)) {
            firstPlayer.setWinsCount(firstPlayer.getWinsCount() + 1);
            secondPlayer.setLosesCount(secondPlayer.getLosesCount() + 1);
        } else {
            secondPlayer.setWinsCount(secondPlayer.getWinsCount() + 1);
            firstPlayer.setLosesCount(firstPlayer.getLosesCount() + 1);
        }

        playersRepository.update(firstPlayer);
        playersRepository.update(secondPlayer);
        System.out.println("Остановка сервера");
    }

    // ищет игрока по никнейму, если такой игрок был - она меняет его IP
    // если игрока не было, она создает нового и сохраняет его
    private Player checkIxExists(String ip, String nickname) {
        Player result;

        Optional<Player> playerOptional = playersRepository.findByNickname(nickname);
        // если игрока под таким именем нет
        if (!playerOptional.isPresent()) {
            // создаем игрока
            Player player = Player.builder()
                    .ip(ip)
                    .nickname(nickname)
                    .losesCount(0)
                    .winsCount(0)
                    .points(0)
                    .build();
            // сохраняем его в репозитории
            playersRepository.save(player);
            result = player;
        } else {
            Player player = playerOptional.get();
            player.setIp(ip);
            playersRepository.update(player);
            result = player;
        }

        return result;
    }
}
